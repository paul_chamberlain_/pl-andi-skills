"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const andiSkills = require("andiskills");
class Skill {
    constructor() {
        var self = this;
    }
    initialize() {
        var initializationResult = {
            successFullyInitialized: true
        };
        return Promise.resolve(initializationResult);
    }
    shouldIRun(skillContext) {
        return Promise.resolve({ shouldIRun: true });
    }
    run(skillContext) {
        return Promise.resolve()
            .then((result) => {
            return skillContext.putMessage({ name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued }, { message: "", category: "" });
        }).catch((err) => {
            return skillContext.putMessage({ name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued }, { message: "", category: "" });
        });
    }
}
exports.Skill = Skill;
//# sourceMappingURL=skill.template.js.map