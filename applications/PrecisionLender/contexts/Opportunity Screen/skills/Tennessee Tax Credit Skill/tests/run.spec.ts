import * as assert from 'assert';
import * as andiSkills from 'andiskills';
import _ = require('underscore');
import { run } from '../run';

class RequestService {
    public get = (url: string, headers: any): Promise<any> =>{
        return this.makeRequest(url, headers, 'GET', null, false).then((res) => {
            return res;
        })
    }

    public post = (url: string, headers: any, body: any, stringifyBodyToJson: boolean = true): Promise<any> =>{
        return this.makeRequest(url, headers, 'POST', body, stringifyBodyToJson);
    }

    // Assumes content-type is json.
    private makeRequest = function (url: string, headers: any, method: string, body: any, stringifyBodyToJson: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }
}

function getContext():andiSkills.ISkillContext{
    var request = new RequestService();
    var skillContext = {

        event: {
            eventType: andiSkills.EventTypes.ApplicationEvent, 
            rawText: "",
            applicationEventName: "log-engine-change",
            applicationEventData: {
                engineModel: {
                    commercialLoanAccounts: [{product: {productFamilyId: "7fcc1988-cc2a-4150-9a89-b3a2bdc9a8a8"}}]
                }
            },
            id: "",
            organizationId: "01e0de4f-9ff3-4ed3-9544-5e2208bfbba3",
            applicationId: "",
            context: "",
            conversationId: "",
            eventMetaData: null,
            userId: "thepner",
            inboundChannel:{
                channelType: andiSkills.ChannelTypes.ImmediateConversational
            } as andiSkills.ISkillChannel
        },        
        organizationId:"",
        userId:"",
        externalLibraries:{_: _},
        powers:{
            andi:{
                request:{
                     get:request.get,
                     post:request.post
                }, 
                fieldTag: {
                    sendFieldTag: null//function(fieldTagType: andiSkills.FieldTagTypes, key: string, displayOrder: number, text: string, fields: string[], section: any, shouldIShow: (api: any, model: any, compareHash?: any, custom?: any) => boolean, action?: (api: any, model: any, custom?: any) => void, compareHash?: number, custom?: any): Promise<andiSkills.ISkillActivity>
                }
                
            }
        } as andiSkills.IPowerSet,
        availableOutputChannels:[],
        skillMetadata:{} as andiSkills.ISkillMetadata,
        skillConfig:{},
        putMessage:function():Promise<andiSkills.ISkillActivity>{
            return Promise.resolve({} as andiSkills.ISkillActivity);
        },
        log(message:any):void{
        },
        
    } as andiSkills.ISkillContext;
    return skillContext
}

describe("Show Message for Bank of Tennessee - Tennessee Tax Credit", () => {
    describe ("Run Tests",()=>{

        // Works for Bank of Tennessee
        it("If I run, the output message is correctly set", (done) => {
            var testMessage = null;
            var context = getContext();
            context.event = context.event as andiSkills.IApplicationEvent;
            context.organizationId = "01e0de4f-9ff3-4ed3-9544-5e2208bfbba3";
            context.event.applicationEventData.engineModel.commercialLoanAccounts[0].product.id = "7fcc1988-cc2a-4150-9a89-b3a2bdc9a8a8";
            context.powers.andi.fieldTag.sendFieldTag = function(fieldTagType: andiSkills.FieldTagTypes, key: string, displayOrder: number, text: string, fields: string[], section: any, shouldIShow: (api: any, model: any, compareHash?: any, custom?: any) => boolean, action?: (api: any, model: any, custom?: any) => void, compareHash?: number, custom?: any):Promise<andiSkills.ISkillActivity>{
                testMessage = text;
                return Promise.resolve({id:null, conversationId:null, channel:null, message :{ message : {attachments:testMessage}} } as andiSkills.ISkillActivity);
            };
            run(context)
            .then((result)=>{
                assert.equal(result.message.message.attachments, "Put the tax credit in the upfront or annual fee section.  Contact Denise Bolinger for annual fee amount.");
                done();
            });
        });

        // Works for First Developer Bank
        it("If I run, the output message is correctly set", (done) => {
            var testMessage = null;
            var context = getContext();
            context.event = context.event as andiSkills.IApplicationEvent;
            context.organizationId = "a9cdadd4-1324-4a7a-842e-fe8fc9fe37f1";
            context.event.applicationEventData.engineModel.commercialLoanAccounts[0].product.id = "4b0fa9ff-31df-4113-97ce-160563ac3f69";
            context.powers.andi.fieldTag.sendFieldTag = function(fieldTagType: andiSkills.FieldTagTypes, key: string, displayOrder: number, text: string, fields: string[], section: any, shouldIShow: (api: any, model: any, compareHash?: any, custom?: any) => boolean, action?: (api: any, model: any, custom?: any) => void, compareHash?: number, custom?: any):Promise<andiSkills.ISkillActivity>{
                testMessage = text;
                return Promise.resolve({id:null, conversationId:null, channel:null, message :{ message : {attachments:testMessage}} } as andiSkills.ISkillActivity);
            };
            run(context)
            .then((result)=>{
                assert.equal(result.message.message.attachments, "Put the tax credit in the upfront or annual fee section.  Contact Denise Bolinger for annual fee amount.");
                done();
            });
        });

    });
});