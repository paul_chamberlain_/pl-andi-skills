import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'
import { IApplicationEvent, EventTypes } from "andiskills";

describe("Bank of Tennessee - Tennessee Tax Credit: ", () => {
    describe("ShouldIRun Tests", () => {
        var loggedMessage = null;
        var skillContext = {
            organizationId: "01e0de4f-9ff3-4ed3-9544-5e2208bfbba3",
            event: {
                eventType: EventTypes.ApplicationEvent, 
                applicationEventName: "log-engine-change",
                applicationEventData: {
                    engineModel: {
                        commercialLoanAccounts: [{product: {productFamilyId: "7fcc1988-cc2a-4150-9a89-b3a2bdc9a8a8"}}]
                    }
                },
                id:"sdf",
                organizationId:"01e0de4f-9ff3-4ed3-9544-5e2208bfbba3",
                applicationId:"sd",
                context:"sd",
                conversationId:"lkj",
                eventMetaData:null,
                userId:"thepner",
                inboundChannel:null
            },
            log(message:any):void{
                loggedMessage = message;
            }
        } as andiSkills.ISkillContext;

        // Defines a Mocha unit test
        it("Logs that it tried to run", (done) => {           
            console.log(skillContext.event.eventType)
            shouldIRun(skillContext)
            .then((result)=>{
                assert.notEqual(loggedMessage,null);
                done();
            });
        });


        it("Checks if returns false when applicationEventName is not log-engine-change", (done) => {           
            skillContext.event = skillContext.event as andiSkills.IApplicationEvent;
            skillContext.event.applicationEventName = "";
            shouldIRun(skillContext)
            .then((result)=>{
                assert.equal(result.shouldIRun, false);
                done();
            });
        });

        it("Checks if returns true when applicationEventName is log-engine-change w/ correct productId", (done) => {  
            skillContext.event = skillContext.event as andiSkills.IApplicationEvent;   
            skillContext.event.applicationEventName = "log-engine-change";      
            shouldIRun(skillContext)
            .then((result)=>{
                assert.equal(result.shouldIRun,true);
                done();
            });
        });

        it("Checks if returns false when applicationEventName is log-engine-change w/ wrong productFamilyId", (done) => {      
            skillContext.event = skillContext.event as andiSkills.IApplicationEvent;
            skillContext.event.applicationEventName = "log-engine-change";    
            skillContext.event.applicationEventData.engineModel.commercialLoanAccounts[0].product.productFamilyId = "";            
            shouldIRun(skillContext)
            .then((result)=>{
                assert.equal(result.shouldIRun, false);
                done();
            });
        });
        
        it("Checks if returns true when productFamilyId is for First Developer Bank", (done) => {      
            skillContext.event = skillContext.event as andiSkills.IApplicationEvent;
            skillContext.event.applicationEventName = "log-engine-change";    
            skillContext.event.applicationEventData.engineModel.commercialLoanAccounts[0].product.productFamilyId = "4b0fa9ff-31df-4113-97ce-160563ac3f69";
            shouldIRun(skillContext)
            .then((result)=>{
                assert.equal(result.shouldIRun,true);
                done();
            });
        });


    });
});