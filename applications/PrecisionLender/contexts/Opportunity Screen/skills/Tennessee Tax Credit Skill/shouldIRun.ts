import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'
import _ = require('underscore');

export function shouldIRun(skillContext: andiSkills.ISkillContext): Promise<andiSkills.IShouldIRunResponse> {
    skillContext.log("Checking if I should run");
    let productFamilyIds = skillContext.skillConfig.productFamilyIds.split(',');

    // Test that the Event is a PrecisionLender Application Event
    let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    let data = event.applicationEventData;
    if (event.applicationEventName === undefined || event.applicationEventName.indexOf("log-engine-change") === -1) {
        return Promise.resolve({ shouldIRun: false });
    }

    if (!data.engineModel.commercialLoanAccounts || data.engineModel.commercialLoanAccounts.length < 1)
        return Promise.resolve({ shouldIRun: false });


    let claFamily = data.engineModel.commercialLoanAccounts.filter(cla => {
        return productFamilyIds.indexOf(cla.product.productFamilyId) > -1;
    });
                
    if (!claFamily ||  claFamily.length < 1)
        return Promise.resolve({ shouldIRun: false });

    return Promise.resolve({ shouldIRun: true });
}