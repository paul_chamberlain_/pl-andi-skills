import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'

export function run(skillContext: andiSkills.ISkillContext) : Promise<andiSkills.ISkillActivity>{
    let productFamilyIds = skillContext.skillConfig.productFamilyIds.split(',');
    let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    let eventData = event.applicationEventData;
    let claFamily = eventData.engineModel.commercialLoanAccounts.filter(cla => {
        return productFamilyIds.indexOf(cla.product.productFamilyId) > -1;
    });
    let claIds = [];
    claFamily.forEach(cla => {
        claIds.push(cla.id);
    });
    // Andi Button Message
    return skillContext.powers.andi.fieldTag.sendFieldTag(
        andiSkills.FieldTagTypes.Info
        , "BoT_Tennessee_Tax_Credit" // What is this for?
        , 10000
        , "Put the tax credit in the upfront or annual fee section. Contact Denise Bolinger for annual fee amount."
        , ["fees", "feesInitialDollars"]
        , null
        , function (api, model, compareHash, custom) {
            let foundClaProductFamilyId = false;
            if (custom && custom.loanIds && custom.loanIds.length > 0) {
                //model.applicationEventData.contextId always represents the current loan id on the opportunity page
                foundClaProductFamilyId = custom.loanIds.indexOf(model.applicationEventData.contextId) > -1;
            }
            return foundClaProductFamilyId;
        }
        , null
        , null
        , { loanIds: claIds });

}
