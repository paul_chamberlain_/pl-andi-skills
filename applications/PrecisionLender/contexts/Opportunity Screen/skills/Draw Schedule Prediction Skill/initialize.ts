import * as andiSkills from 'andiskills';

export function initialize(): Promise<andiSkills.ISkillInitializationResult>{
    //initialize block is executed when your skill is first time installed
    //start here  
    var initializationResult = {
        successFullyInitialized:true
    } as andiSkills.ISkillInitializationResult;
    
    return Promise.resolve(initializationResult);
    //end here
}