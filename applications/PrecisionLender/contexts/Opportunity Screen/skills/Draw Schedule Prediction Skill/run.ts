import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal';
import * as _ from 'underscore';

export function run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
    var event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    var data = event.applicationEventData;
    var cla = _.find<any>(data.engineModel.commercialLoanAccounts, function(cla) {
        return cla.id === data.contextId;
    });

    var sb: string[] = [];
    sb.push(cla.amount);
    sb.push(cla.maturity);
    sb.push(data.opportunityDetails.relationshipId);
    sb.push(data.contextId);
    var hashable: string = sb.join(''),
        hash = 0,
        i;
    if (hashable.length !== 0) {
        for (i = 0; i < hashable.length; i++) {
            var thisChar = hashable.charCodeAt(i);
            hash = ((hash << 5) - hash) + thisChar;
            hash = hash & hash; // Convert to 32bit integer
        }
    }

    return getResponseFromMLService(cla.amount,
            cla.maturity,
            data.opportunityDetails.relationshipId,
            event.organizationId,
            skillContext.skillConfig.apiHost,
            skillContext.skillConfig.apiKey,
            skillContext.skillConfig.apiPath)
        .then(function(result) {
            //handle response                
            var drawSchedulePrediction = result.Results.output1.value.Values[0];
            var recommendedSchedule = [];
            var over70Percent = false;
            var previousValue = 0.0;
            var recommendedCount = 0;
            for (var i = 0; i < drawSchedulePrediction.length; i++) {
                if (Number(drawSchedulePrediction[i]) > 70)
                    over70Percent = true;

                if (Number(drawSchedulePrediction[i]) > Number(previousValue)) {
                    var drawPercent = (drawSchedulePrediction[i] - previousValue) / 100.0;
                    var drawAmount = drawPercent * cla.amount;
                    var drawMonth = i;

                    previousValue = drawSchedulePrediction[i];
                    recommendedSchedule[recommendedCount] = { drawMonth: drawMonth, drawAmount: drawAmount };
                    recommendedCount++;
                }
            }
            //recommended schedule will need to be processed through the DrawModel on the skill service side.  
            //Previously that was done in the above for loop, but we don't have access to it here.

            skillContext.log('put message to field tag channel');
            return skillContext.powers.andi.fieldTag.sendFieldTag(andiSkills.FieldTagTypes.Info,
                "SKILL_PredictDrawSchedule",
                10000,
                "Based on what you\'ve told me, I can set a draw schedule for you",
                ['draws'],
                null,
                function(api, model, compareHash, custom) {
                    var data = model.applicationEventData;
                    var cla = _.find<any>(data.engineModel.commercialLoanAccounts, function(cla) {
                        return cla.id === data.contextId;
                    });

                    if (!cla || !(cla.paymentType === 8))
                        return false;

                    if (cla.maturity > 60)
                        return false;

                    var sb: string[] = [];
                    sb.push(cla.amount);
                    sb.push(cla.maturity);
                    sb.push(data.opportunityDetails.relationshipId);
                    sb.push(data.contextId);
                    var hashable: string = sb.join(''),
                        hash = 0,
                        i;
                    if (hashable.length !== 0) {
                        for (i = 0; i < hashable.length; i++) {
                            var thisChar = hashable.charCodeAt(i);
                            hash = ((hash << 5) - hash) + thisChar;
                            hash = hash & hash; // Convert to 32bit integer
                        }
                    }
                    if (hash !== compareHash)
                        return false;

                    var event = model.applicationEventData;
                    var claApi = api['scenarios'][event.engineModel.id]['commercialLoanAccounts'][event.contextId].commercialLoanAccount;
                    var recommendedSchedule = [];
                    _.each<any>(custom.schedule, function(draw, index) {
                        recommendedSchedule[index] = claApi.getDrawModelFromValues(draw.drawMonth, draw.drawAmount);
                    });

                    var proposedScheduleAmortization = claApi.getCustomerAmortizationWithProposedDrawSchedule(recommendedSchedule);
                    var avgDistancePercent = claApi.getAmortizationDistance(proposedScheduleAmortization);
                    if (custom.over70Percent && avgDistancePercent >= 0.05) {
                        claApi.setAndiDrawScheduleSuggestion(custom.drawSchedulePrediction);
                        claApi.setAndiDrawScheduleSuggestionAmortization(proposedScheduleAmortization);
                        return true;
                    }
                    else {
                        claApi.setAndiDrawScheduleSuggestion(null);
                        claApi.setAndiDrawScheduleSuggestionAmortization(null);
                        return false;
                    }
                },
                function(api, model) {
                    var event = model.applicationEventData;
                    api['scenarios'][event.engineModel.id]['commercialLoanAccounts'][event.contextId].commercialLoanAccount.openDrawScheduleDialog();
                    return false;
                },
                hash,
                { schedule: recommendedSchedule, over70Percent: over70Percent, drawSchedulePrediction: drawSchedulePrediction });
        });
}

export function getResponseFromMLService(commitment: number, maturity: number, relationshipId: string, clientId: string, apiHost: string, apiKey: string, apiPath: string): Promise<any> {
    return new Promise((resolve, reject) => {
        var https = require("https");
        const Host = apiHost;
        const APIKey = apiKey;
        const path = apiPath;

        if (!relationshipId) {
            relationshipId = "null";
        }
        else {
            relationshipId = relationshipId.toUpperCase();
        }

        clientId = clientId.toUpperCase();

        var info = {
            "Inputs": {
                "input1": {
                    "ColumnNames": [
                        "Commitment",
                        "Maturity",
                        "RelationshipName",
                        "ClientId",
                        "Hash"
                    ],
                    "Values": [
                        [
                            commitment,
                            maturity,
                            relationshipId,
                            clientId,
                            ""
                        ]
                    ]
                }
            },
            "GlobalParameters": {}
        }
        var body = JSON.stringify(info);

        var post_options = {
            host: Host,
            path: path,
            method: 'POST',
            accept: 'application/json',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': body.length,
                Authorization: 'Bearer ' + APIKey
            }
        };

        const req = https.request(post_options,
            function (res) {
                res.setEncoding('utf8');
                const data = [];
                res.on('data', (chunk) => data.push(chunk));

                res.on('end', () => {
                    try {
                        if (res.headers['content-type'] !== undefined && res.headers['content-type'].indexOf('json') > -1)
                            resolve(JSON.parse(data.join('')));
                        resolve(data.join(''));
                    } catch (e) {
                        reject(e);
                    }
                });
                req.on('error', (err) => {
                    reject(err);
                });
            });
        req.on('error', (err) => {
            reject(err);
        });
        req.write(body);
        req.end();
    });
}