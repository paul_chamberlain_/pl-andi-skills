import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'

export function shouldIRun(skillContext:andiSkills.ISkillContext):Promise<andiSkills.IShouldIRunResponse>{
    //shouldIRun is executed everytime an event is received by the dispatcher
    //start here
    skillContext.log("shouldIRun: add your comments here..");
    
    //check if your event is an opportunity change event
    let shouldIRun = false;
    
    //return false if you don't want to run your skill
    if(!shouldIRun)
        return skillContext.powers.andi.shouldIRun.shouldIRunFalse(); 

    //return true 
    return skillContext.powers.andi.shouldIRun.shouldIRunTrue();
    //end here
}