import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal';

// run is executed if shouldIRun returns true
export function run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
    let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    let eventData = event.applicationEventData;
    var depositProductFamilyIds = skillContext.skillConfig.depositProductFamilyIds.split(",");
    var otherProductFamilyIds = skillContext.skillConfig.otherProductFamilyIds.split(",");
    let depositAccount = eventData.engineModel.depositAccounts.find(deposit => {
        return depositProductFamilyIds.indexOf(deposit.product.productFamilyId) > -1 && deposit.id === event.currentContext;
    });

    // this is a safety message, but we should NEVER use this
    let tagMsg = "This product also needs to have new TMS fees to offset the estimated ECR.";
    if (depositAccount.product.name)
        tagMsg = depositAccount.product.name + " product also needs to have new TMS fees to offset the estimated ECR.";
    let depositId = depositAccount.id;

    // Andi Button Message
    return skillContext.powers.andi.fieldTag.sendFieldTag(
        andiSkills.FieldTagTypes.Info
        , "TD-DDA-with-TMS"
        , 10000
        , tagMsg
        , ["averageBalance"]
        , null
        , function (api, model, compareHash, custom) {
            var data = model.applicationEventData;
            let foundDepositProductFamilyId = false;
            if (custom && custom.depositId) {
                var depositFound = data.engineModel.depositAccounts.find(function (deposit) {
                    return deposit.id === custom.depositId;
                });
                if (depositFound)
                    foundDepositProductFamilyId = true;
            }
            //check if other accounts already have right product then don't show
            if (custom && custom.otherProductFamilyIds && custom.otherProductFamilyIds.length > 0) {
                var otherFound = data.engineModel.otherFeesAccounts.find(function (other) {
                    return custom.otherProductFamilyIds.indexOf(other.product.productFamilyId) > -1;
                });
                if (otherFound)
                    foundDepositProductFamilyId = false;
            }
            return foundDepositProductFamilyId;
        }
        , null
        , null
        , { depositId: depositId, otherProductFamilyIds: otherProductFamilyIds });
}