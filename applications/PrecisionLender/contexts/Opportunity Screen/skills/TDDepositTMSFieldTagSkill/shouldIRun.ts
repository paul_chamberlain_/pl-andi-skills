import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal';
import _ = require('underscore');

// shouldIRun is executed everytime an event is received by the dispatcher
export function shouldIRun(skillContext: andiSkills.ISkillContext): Promise<andiSkills.IShouldIRunResponse> {
    skillContext.log("Checking if I should run");

    let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    let data = event.applicationEventData;

    if (event.applicationEventName === undefined || event.applicationEventName.indexOf("log-engine-change-deposit") === -1) {
        return Promise.resolve({ shouldIRun: false });
    }

    if (!event.currentContext) {
        return Promise.resolve({ shouldIRun: false });
    }

    if (!data.engineModel.depositAccounts || data.engineModel.depositAccounts.length < 1) {
        return Promise.resolve({ shouldIRun: false });
    }

    var otherProductFamilyIds = skillContext.skillConfig.otherProductFamilyIds.split(",");
    if (data.engineModel.otherFeesAccounts && data.engineModel.otherFeesAccounts.length > 0) {
        let otherAccounts = data.engineModel.otherFeesAccounts.filter(other => {
            return otherProductFamilyIds.indexOf(other.product.productFamilyId) > -1;
        });
        if (otherAccounts)
            return Promise.resolve({ shouldIRun: false });
    }
    var depositProductFamilyIds = skillContext.skillConfig.depositProductFamilyIds.split(",");
    let depositAccount = data.engineModel.depositAccounts.find(deposit => {
        return depositProductFamilyIds.indexOf(deposit.product.productFamilyId) > -1 && deposit.id === event.currentContext;
    });

    if (depositAccount)
        return Promise.resolve({ shouldIRun: true });

    return Promise.resolve({ shouldIRun: false });
}