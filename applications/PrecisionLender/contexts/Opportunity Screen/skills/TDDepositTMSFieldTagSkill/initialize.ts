import * as andiSkills from 'andiskills';

// initialize is executed when your skill is first time installed
export function initialize(): Promise<andiSkills.ISkillInitializationResult> {
    
    var initializationResult = {
        successFullyInitialized:true
    } as andiSkills.ISkillInitializationResult;
    
    return Promise.resolve(initializationResult);
}