import * as assert from 'assert';
import { run } from '../run';
import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal';

describe("Run Tests", () => {

    // Defines a Mocha unit test
    it("Logs that it ran", (done) => {

        var skillContext = andiExternal.PrecisionLenderMocks.Mocks.mockSkillContextForPrecisionLenderOpportunityChange() as andiSkills.ISkillContext;
        let data = skillContext.powers.precisionLender.opportunity.getOpportunityChangeEventData(skillContext);
        let opportunityData = data as andiExternal.PrecisionLenderOpportunityChangeEvent.OpportunityChangeEvent;
        data.engineModel.depositAccounts = [{}];        

        run(skillContext)
        .then((result)=>{
            assert.notEqual(result,null);
            done();
        });
        
    });

});
