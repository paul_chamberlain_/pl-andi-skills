import * as assert from 'assert';
import { initialize } from '../initialize';
import * as andiSkills from 'andiskills';

describe("Example Email",()=>{

    // Defines a Mocha unit test
    describe("Initialize Tests", () => {
        it("returns as successfully initialized", (done) => {

            initialize()
            .then((result)=>{
                assert.equal(result.successFullyInitialized,true);
                done();
            });
            
        });
    });

});