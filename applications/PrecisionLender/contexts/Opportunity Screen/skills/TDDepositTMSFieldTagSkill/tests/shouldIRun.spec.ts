import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal';

describe("ShouldIRun Tests", () => {

    // Defines a Mocha unit test
    it("Logs that it tried to run", (done) => {

        var skillContext = andiExternal.PrecisionLenderMocks.Mocks.mockSkillContextForPrecisionLenderOpportunityChange() as andiSkills.ISkillContext;
        let data = skillContext.powers.precisionLender.opportunity.getOpportunityChangeEventData(skillContext);
        let opportunityData = data as andiExternal.PrecisionLenderOpportunityChangeEvent.OpportunityChangeEvent;
        data.engineModel.depositAccounts = undefined;  

        shouldIRun(skillContext)
        .then((result)=>{
            assert.equal(result.shouldIRun,false);
            done();
        });

    });

    // Defines a Mocha unit test
    it("Logs that it returned true when deposit accounts are present", (done) => {

        var skillContext = andiExternal.PrecisionLenderMocks.Mocks.mockSkillContextForPrecisionLenderOpportunityChange() as andiSkills.ISkillContext;
        let data = skillContext.powers.precisionLender.opportunity.getOpportunityChangeEventData(skillContext);
        let opportunityData = data as andiExternal.PrecisionLenderOpportunityChangeEvent.OpportunityChangeEvent;
        data.engineModel.depositAccounts = [{}];    

        shouldIRun(skillContext)
        .then((result)=>{
            assert.equal(result.shouldIRun,true);
            done();
        });

    });
});
