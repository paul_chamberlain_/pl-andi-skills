"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const shouldIRun_1 = require("../shouldIRun");
const andiskills_1 = require("andiskills");
describe("TTC MVP", () => {
    describe("ShouldIRun Tests", () => {
        // Defines a Mocha unit test
        it("Logs that it tried to run", (done) => {
            var loggedMessage = null;
            var skillContext = {
                event: { eventType: andiskills_1.EventTypes.ApplicationEvent,
                    applicationEventName: "NewOpportunityCreated",
                    applicationEventData: "more",
                    id: "sdf",
                    organizationId: "sdf",
                    applicationId: "sd",
                    context: "sd",
                    conversationId: "lkj",
                    eventMetaData: null,
                    userId: "ljack",
                    inboundChannel: null
                },
                log(message) {
                    loggedMessage = message;
                }
            };
            shouldIRun_1.shouldIRun(skillContext)
                .then((result) => {
                assert.notEqual(loggedMessage, null);
                done();
            });
        });
        it("Checks if it returns true when applicationEventName is NewOpportunityCreated", (done) => {
            var loggedMessage = null;
            var skillContext = {
                event: { eventType: andiskills_1.EventTypes.ApplicationEvent,
                    applicationEventName: "NewOpportunityCreated",
                    applicationEventData: "more",
                    id: "sdf",
                    organizationId: "sdf",
                    applicationId: "sd",
                    context: "sd",
                    conversationId: "lkj",
                    eventMetaData: null,
                    userId: "ljack",
                    inboundChannel: null
                },
                log(message) {
                    loggedMessage = message;
                }
            };
            var shouldIRunResults = shouldIRun_1.shouldIRun(skillContext)
                .then((result) => {
                assert.equal(result.shouldIRun, true);
                done();
            });
        });
    });
});
//# sourceMappingURL=shouldIRun.spec.js.map