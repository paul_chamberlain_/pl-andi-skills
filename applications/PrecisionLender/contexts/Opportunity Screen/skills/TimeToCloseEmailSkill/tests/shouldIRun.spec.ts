import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';
import { IApplicationEvent, EventTypes } from "andiskills";

describe("TTC MVP", () => {
    describe("ShouldIRun Tests", () => {

        // Defines a Mocha unit test
        it("Logs that it tried to run", (done) => {
            var loggedMessage = null;
            
            var skillContext = {
                event: {eventType:EventTypes.ApplicationEvent,
                    applicationEventName:"NewOpportunityCreated",
                    applicationEventData:"more",
                    id:"sdf",
                    organizationId:"sdf",
                    applicationId:"sd",
                    context:"sd",
                    conversationId:"lkj",
                    eventMetaData:null,
                    userId:"ljack",
                    inboundChannel:null
                },
                log(message:any):void{
                    loggedMessage = message;
                }
            } as andiSkills.ISkillContext;
            
            
            shouldIRun(skillContext)
            .then((result)=>{
                assert.notEqual(loggedMessage,null);
                done();
            });
        });
        
        
        it("Checks if it returns true when applicationEventName is NewOpportunityCreated", (done) => {
            var loggedMessage = null;
            var skillContext = {
                event: {eventType:EventTypes.ApplicationEvent,
                    applicationEventName:"NewOpportunityCreated",
                    applicationEventData:"more",
                    id:"sdf",
                    organizationId:"sdf",
                    applicationId:"sd",
                    context:"sd",
                    conversationId:"lkj",
                    eventMetaData:null,
                    userId:"ljack",
                    inboundChannel:null
                },
                log(message:any):void{
                    loggedMessage = message;
                }
            } as andiSkills.ISkillContext;
            
            var shouldIRunResults = shouldIRun(skillContext)

            .then((result)=>{
                assert.equal(result.shouldIRun,true);
                done();
            });
        });
    });
});

