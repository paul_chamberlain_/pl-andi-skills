"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const run_1 = require("../run");
const andiSkills = require("andiskills");
const _ = require("underscore");
class RequestService {
    constructor() {
        this.get = (url, headers) => {
            return this.makeRequest(url, headers, 'GET', null, false).then((res) => {
                return res;
            });
        };
        this.post = (url, headers, body, stringifyBodyToJson = true) => {
            return this.makeRequest(url, headers, 'POST', body, stringifyBodyToJson);
        };
        // Assumes content-type is json.
        this.makeRequest = function (url, headers, method, body, stringifyBodyToJson) {
            return new Promise((resolve, reject) => {
                var http = url.startsWith('https') ? require('https') : require('http');
                var port = url.startsWith('https') ? '443' : '80';
                url = url.replace('http://', '').replace('https://', '');
                var portMatch = /\:\d{2,5}/.exec(url);
                if (portMatch !== null && portMatch.length > 0) {
                    url = url.replace(portMatch[0], '');
                    port = portMatch[0].replace(':', '');
                }
                var hostname = url.substring(0, url.indexOf('/'));
                var path = url.substring(url.indexOf('/'));
                var options = {
                    hostname: hostname,
                    path: path,
                    port: port,
                    method: method,
                    headers: _.extend({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }, headers)
                };
                const req = http.request(options, function (res) {
                    res.setEncoding('utf8');
                    const data = [];
                    res.on('data', (chunk) => data.push(chunk));
                    res.on('end', () => {
                        try {
                            if (res.headers['content-type'] !== undefined && res.headers['content-type'].indexOf('json') > -1)
                                resolve(JSON.parse(data.join('')));
                            resolve(data.join(''));
                        }
                        catch (e) {
                            var err = e + ' Request Host: ' + options.hostname;
                            reject(err);
                        }
                    });
                });
                req.on('error', (err) => reject(err));
                // write data to request body
                if (body)
                    req.write(stringifyBodyToJson ? JSON.stringify(body) : body);
                req.end();
            });
        };
    }
}
function getContext() {
    var request = new RequestService();
    return {
        event: {
            id: "",
            organizationId: "",
            applicationId: "",
            context: "",
            conversationId: "",
            userId: "",
            inboundChannel: {
                channelType: andiSkills.ChannelTypes.ImmediateConversational
            },
            eventType: andiSkills.EventTypes.InboundNaturalLanguage,
            rawText: ""
        },
        organizationId: "",
        userId: "",
        externalLibraries: { _: _ },
        powers: {
            andi: {
                request: {
                    get: request.get,
                    post: request.post
                }
            }
        },
        availableOutputChannels: [],
        skillMetadata: {},
        skillConfig: {},
        putMessage: function () {
            return Promise.resolve({});
        },
        log(message) {
        },
    };
}
describe("TTC MVP", () => {
    describe("getPrediction Tests", () => {
        it("If I pass in my favorite combo, I get 49.564", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "85872002-AEAB-4E83-8F09-6E5569641219";
            context.userId = "6464AC42-C51D-474F-ABEC-6BE3A3169611";
            run_1.getPrediction(context)
                .then((result) => {
                assert.equal(result, '49.5640162962893');
                done();
            });
        });
        it("If I pass in garbage for client & owner, I get 50", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "garbage garbage garbage garbage";
            context.userId = "garbage garbage garbage garbage";
            run_1.getPrediction(context)
                .then((result) => {
                assert.equal(result, '50');
                done();
            });
        });
        it("If I pass in my fave client but garbage for owner, I get 65.18765", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "85872002-AEAB-4E83-8F09-6E5569641219";
            context.userId = "garbage garbage garbage garbage";
            run_1.getPrediction(context)
                .then((result) => {
                assert.equal(result, '65.1876505293392');
                done();
            });
        });
    });
    describe("Run Tests", () => {
        // Defines a Mocha unit test
        it("If I pass in whatever, a message is Queued to ljackson@precisionlender.com", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var testEmail = "ljackson@precisionlender.com";
            var emailSent = false;
            var context = getContext();
            context.putMessage = function (channel, message, action) {
                testMessage = message.message;
                return Promise.resolve({});
            };
            //context.organizationId = "0B8359DE-5B0C-4A3F-83FD-9CB56CF2D19A";
            //context.userId = "E6E1B668-25DC-4471-AB2D-ECAB08730537";
            context.organizationId = "85872002-AEAB-4E83-8F09-6E5569641219";
            context.userId = "6464AC42-C51D-474F-ABEC-6BE3A3169611";
            console.log(context.putMessage);
            run_1.run(context)
                .then((result) => {
                var email = testMessage;
                console.log(email.messageStatus);
                console.log(email.toAddress[0].emailAddress);
                assert.equal(email.messageStatus, 'Queued');
                assert.equal(email.toAddress[0].emailAddress, testEmail);
                done();
                //console.log(email.fromAddress);
            });
        });
    });
});
//# sourceMappingURL=run.spec.js.map