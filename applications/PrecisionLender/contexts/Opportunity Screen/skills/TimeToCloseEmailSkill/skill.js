"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const andiSkills = require("andiskills");
class Skill {
    constructor() {
        var self = this;
    }
    initialize() {
        var initializationResult = {
            successFullyInitialized: true
        };
        return Promise.resolve(initializationResult);
    }
    shouldIRun(skillContext) {
        if (skillContext.event.eventType != andiSkills.EventTypes.ApplicationEvent) {
            return Promise.resolve({ shouldIRun: false });
        }
        let anEvent = skillContext.event;
        if (anEvent.applicationEventName !== "NewOpportunityCreated") {
            return Promise.resolve({ shouldIRun: false });
        }
        skillContext.log("Checking if I should run");
        return Promise.resolve({ shouldIRun: true });
    }
    run(skillContext) {
        return this.getPrediction(skillContext)
            .then((result) => {
            var userModel = new andiSkills.EmailUserModel("Laura", "ljackson@precisionlender.com");
            //console.log(userModel);
            var message = new andiSkills.EmailMessageModel(andiSkills.EmailFrequencyTypes.OneMinute, [userModel], "andi@precisionlender.com", "Subject - TimeToClose Skill Has Been Run", "<strong>html Predicted TimeToClose = </strong> Prediction: " + result, `footer`);
            console.log(message.body);
            return skillContext.putMessage({ name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued }, { message: message, category: 'TTC MVP' });
        }).catch((err) => {
            var userModel = new andiSkills.EmailUserModel("Laura", "ljackson@precisionlender.com");
            //console.log(userModel);
            var message = new andiSkills.EmailMessageModel(andiSkills.EmailFrequencyTypes.OneMinute, [userModel], "andi@precisionlender.com", "Subject - TimeToClose Skill Has errored out", JSON.stringify(err), `footer`);
            return skillContext.putMessage({ name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued }, { message: message, category: 'TTC MVP' });
        });
    }
    getPrediction(skillContext) {
        //var get = skillContext.powers.andi.request.get;
        let key = "thoc4bTNOcsBdBm/ukiMTRzbEzEyuSfDT8PQB9ykTmBd/VgBT8Geo/wDUgX3ufFyvOdFoe8EknNxjk2FRNL9Ug==";
        let URL = "https://ussouthcentral.services.azureml.net/subscriptions/b7b3f313bcc84754aa71a8c9a910841c/services/d1138d23ddf64a44893e6be4fa8e778e/execute?api-version=2.0&format=swagger";
        let BearerKey = "Bearer " + key;
        //skillContext.powers.andi.request.post(url: string, headers: any, data: any): Promise<any> 
        var data = {
            "Inputs": {
                "input1": [
                    {
                        "ClientId": skillContext.organizationId,
                        "OwnerId": skillContext.userId
                    }
                ]
            },
            "GlobalParameters": {}
        };
        console.log(data.Inputs);
        return skillContext.powers.andi.request.post(URL, { "Authorization": BearerKey }, data)
            .then((result) => {
            console.log(result.Results);
            return result.Results.output1[0].prediction;
        });
    }
}
exports.Skill = Skill;
//# sourceMappingURL=skill.js.map