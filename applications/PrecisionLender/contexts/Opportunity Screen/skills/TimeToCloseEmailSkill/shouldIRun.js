"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const andiSkills = require("andiskills");
// The TimeToClose Skill should run every time an opportunity is created.
// The resulting TimeToClose prediction is returned as an InstantAction
function shouldIRun(skillContext) {
    if (skillContext.event.eventType != andiSkills.EventTypes.ApplicationEvent) {
        return Promise.resolve({ shouldIRun: false });
    }
    let anEvent = skillContext.event;
    if (anEvent.applicationEventName == "NewOpportunityCreated") {
        var whetherIShouldRun = true;
    }
    skillContext.log("Checking if I should run");
    return Promise.resolve({ shouldIRun: whetherIShouldRun }); // it's an object with property shouldIRun, set to value of the var shouldIRun(true)
}
exports.shouldIRun = shouldIRun;
//# sourceMappingURL=shouldIRun.js.map