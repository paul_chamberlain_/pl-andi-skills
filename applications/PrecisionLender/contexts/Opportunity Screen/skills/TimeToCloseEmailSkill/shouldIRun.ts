import * as andiSkills from 'andiskills';
import { IApplicationEvent, EventTypes } from "andiskills";

// The TimeToClose Skill should run every time an opportunity is created.
// The resulting TimeToClose prediction is returned as an InstantAction
export function shouldIRun(skillContext:andiSkills.ISkillContext):Promise<andiSkills.IShouldIRunResponse>{
    skillContext.log("Checking if I should run");
    if (skillContext.event.eventType !== andiSkills.EventTypes.ApplicationEvent) {
        return Promise.resolve({shouldIRun: false});
    }
    let anEvent:andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    if (anEvent.applicationEventName === "NewOpportunityCreated") {
        return Promise.resolve({ shouldIRun: true  });   // it's an object with property shouldIRun, set to value of the var shouldIRun(true)
    }

    return Promise.resolve({ shouldIRun: false  });   // it's an object with property shouldIRun, set to value of the var shouldIRun(true)
}