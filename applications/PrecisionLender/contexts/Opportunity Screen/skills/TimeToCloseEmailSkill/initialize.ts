import * as andiSkills from 'andiskills';

export function initialize(): Promise<andiSkills.ISkillInitializationResult>{
    var initializationResult = {
        successFullyInitialized:true
    } as andiSkills.ISkillInitializationResult;
    
    return Promise.resolve(initializationResult);
}