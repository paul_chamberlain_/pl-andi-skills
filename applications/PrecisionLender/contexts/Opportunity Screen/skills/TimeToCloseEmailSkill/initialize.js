"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function initialize() {
    var initializationResult = {
        successFullyInitialized: true
    };
    return Promise.resolve(initializationResult);
}
exports.initialize = initialize;
//# sourceMappingURL=initialize.js.map