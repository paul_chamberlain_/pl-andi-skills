import * as andiSkills from 'andiskills';
import * as _ from 'underscore';

export function shouldIRun(skillContext: andiSkills.ISkillContext): Promise<andiSkills.IShouldIRunResponse> {
    var event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    if (event.eventType !== andiSkills.EventTypes.ApplicationEvent && event.eventType !== andiSkills.EventTypes.ApplicationEvent["ApplicationEvent"])
        return Promise.resolve({ shouldIRun: false });

    if (event.inboundChannel.channelType !== andiSkills.ChannelTypes.ImmediateMessage && event.inboundChannel.channelType !== andiSkills.ChannelTypes.ImmediateMessage["ImmediateMessage"])
        return Promise.resolve({ shouldIRun: false });

    var userIds: [string] = skillContext.skillConfig.userIds;
    var userToRun = _.find(userIds, function(userId) {
        return userId.toLowerCase() === event.userId || userId.toLowerCase() === "all";
    });
    if (!userToRun)
        return Promise.resolve({ shouldIRun: false });

    if (event.applicationEventName === undefined || event.applicationEventName.indexOf("log-engine-change") === -1)
        return Promise.resolve({ shouldIRun: false });

    if (!event.applicationEventData.engineModel.isPipelineScenario)
        return Promise.resolve({ shouldIRun: false });

    var data = event.applicationEventData;
    var latestAccountChange = data.accountChanges[data.accountChanges.length - 1];
    if (latestAccountChange.keyValuePairs.newROE > latestAccountChange.keyValuePairs.targetROE)
        return Promise.resolve({ shouldIRun: false });

    var cla = _.find<any>(data.engineModel.commercialLoanAccounts, function(cla) {
        return cla.id === data.contextId;
    });

    if (cla && cla.adjustableRateIndexName === "Prime" && cla.rateType === 2)
        return Promise.resolve({ shouldIRun: true });

    return Promise.resolve({ shouldIRun: false });
}