import * as andiSkills from 'andiskills';
import * as _ from 'underscore';

export function run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
    var event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    var data = event.applicationEventData;
    var cla = _.find<any>(data.engineModel.commercialLoanAccounts, function(cla) {
        return cla.id === data.contextId;
    });

    function mapTermYears(maturity) {
        if (maturity <= 60)
            return "1 Year";
        else
            return ">= 2 Years";
    }

    function mapCommitRangeBucket(amount) {
        if (amount <= 100000)
            return "< 100K";
        else if (amount < 300000)
            return "100K to <300K";
        else if (amount < 500000)
            return "300K to <500K";
        else if (amount < 1000000)
            return "500K to <1MM";
        else if (amount < 5000000)
            return "1MM to <5MM";
        else if (amount < 10000000)
            return "5MM to < 10MM";
        else if (amount < 25000000)
            return "10MM to < 25MM";
        else if (amount >= 25000000)
            return ">= $25MM";
    }

    function mapPaymentType(paymentType) {
        switch (paymentType) {
            case 4:
                return "Revolvers";
            default:
                return "Term Loans";
        }
    }

    function mapLineOfBusiness(claName) {
        if (claName.replace(/\s/g).toLowerCase().indexOf("realestate"))
            return "Commercial Real Estate";
        else
            return "Commercial Banking";
    }

    var commitRangeString = mapCommitRangeBucket(cla.amount);
    var termMonthsString = mapTermYears(cla.maturity);
    var paymentTypeString = mapPaymentType(cla.paymentType);
    var lineOfBusiness = mapLineOfBusiness(cla.product.name);
    return this.getMLData(skillContext, termMonthsString, commitRangeString, "3a" /*cla.riskRating.rating.toLowerCase()*/, paymentTypeString, "New", "Alabama Region", lineOfBusiness).then((result) => {
        if (result.Results.output1[0].count === "0")
            return;
        var guideline = result.Results.output1[0];
        var sb: string[] = [];
        sb.push(cla.adjustableRateIndexName);
        sb.push(cla.rateType);
        sb.push(cla.amount);
        sb.push(cla.riskRating.riskRatingId);
        sb.push(cla.maturity);
        sb.push(cla.paymentType);

        var hashable: string = sb.join(''),
            hash = 0,
            i;
        if (hashable.length !== 0) {
            for (i = 0; i < hashable.length; i++) {
                var thisChar = hashable.charCodeAt(i);
                hash = ((hash << 5) - hash) + thisChar;
                hash = hash & hash; // Convert to 32bit integer
            }
        }

        return skillContext.powers.andi.fieldTag.sendFieldTag(andiSkills.FieldTagTypes.Info,
            "SKILL_CopyCurrentScenario",
            10000,
            `Based on S&P data I can create a benchmark scenario that may better reach your target with ${guideline["GUIDELINE_UPFRONT_FEE_BPS"] /
            100}% initial fees and with your current Prime index over ${guideline["GUIDELINE_PRIME_BPS"] / 100}%`,
            [],
            null,
            function(api, model, compareHash, custom) {
                var data = model.applicationEventData;
                var cla = _.find<any>(data.engineModel.commercialLoanAccounts, function(cla) {
                    return cla.id === data.contextId;
                });
                if (cla.financialStatement.roe > cla.financialStatement.targetROE)
                    return false;

                var sb: string[] = [];
                sb.push(cla.adjustableRateIndexName);
                sb.push(cla.rateType);
                sb.push(cla.amount);
                sb.push(cla.riskRating.riskRatingId);
                sb.push(cla.maturity);
                sb.push(cla.paymentType);
                var hashable: string = sb.join(''),
                    hash = 0,
                    i;
                if (hashable.length !== 0) {
                    for (i = 0; i < hashable.length; i++) {
                        var thisChar = hashable.charCodeAt(i);
                        hash = ((hash << 5) - hash) + thisChar;
                        hash = hash & hash; // Convert to 32bit integer
                    }
                }
                if (hash !== compareHash)
                    return false;

                return true;
            },
            function (api, model, custom) {
                var data = model.applicationEventData;
                var claFromModel = _.find<any>(data.engineModel.commercialLoanAccounts, function (cla) {
                    return cla.id === data.contextId;
                });
                var scenarioToCopy = api.opportunity.getCurrentScenario();
                var recordIndex;
                var cla = _.find<any>(scenarioToCopy.commercialLoanAccounts(), function(cla, i) {
                    recordIndex = i;
                    return cla.id.id === claFromModel.id;
                });
                api.page.openScenarioPopup();
                var scenarioId = api.opportunity.createCopyScenario(scenarioToCopy);
                var count = -1;
                var claToMod = _.find<any>(api.scenarios[scenarioId.id].commercialLoanAccounts, function() {
                    count++;
                    return recordIndex === count;
                }).commercialLoanAccount;
                claToMod.setInitialFeesPercentOfAmount(custom.guideline["GUIDELINE_UPFRONT_FEE_BPS"] / 10000); // convert from bps
                claToMod.setAdjustableRateSpread(custom.guideline["GUIDELINE_PRIME_BPS"] / 10000); // convert from bps

                return false;
            },
            hash,
            { guideline: guideline });
    });
}

export function getMLData(skillContext: andiSkills.ISkillContext, termYears, sizeDesc, customerRisk, facilityType, originationStatusName, newRegion, lineOfBusiness): Promise<any> {
    let URL = "https://" + skillContext.skillConfig.apiHost + skillContext.skillConfig.apiPath;
    let BearerKey = "Bearer " + skillContext.skillConfig.apiKey;

    var input = {
        "Inputs": {
            "input1": [
                {
                    "TERM_YEARS": termYears,
                    "SIZE_DESC": sizeDesc,
                    "CUSTOMER_RISK": customerRisk,
                    "FACILITY_TYPE_DESC": facilityType,
                    "ORIGINATION_STATUS_NAME": originationStatusName,
                    "NEW_REGION": newRegion,
                    "LINE_OF_BUSINESS": lineOfBusiness
                }
            ]
        },
        "GlobalParameters": {}
    };

    return skillContext.powers.andi.request.post(URL, { "Authorization": BearerKey }, input);
}