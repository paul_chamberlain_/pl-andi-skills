"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function shouldIRun(skillContext) {
    var shouldIRun = true;
    skillContext.log("Checking if I should run");
    return Promise.resolve({ shouldIRun: shouldIRun });
}
exports.shouldIRun = shouldIRun;
//# sourceMappingURL=shouldIRun.js.map