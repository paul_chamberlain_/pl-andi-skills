"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const initialize_1 = require("../initialize");
describe("Initialize Tests", () => {
    // Defines a Mocha unit test
    it("returns as successfully initialized", (done) => {
        initialize_1.initialize()
            .then((result) => {
            assert.equal(result.successFullyInitialized, true);
            done();
        });
    });
});
//# sourceMappingURL=initialize.spec.js.map