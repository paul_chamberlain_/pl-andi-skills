"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const run_1 = require("../run");
describe("Run Tests", () => {
    // Defines a Mocha unit test
    it("Logs that it ran", (done) => {
        var loggedMessage = null;
        var skillContext = {
            log(message) {
                loggedMessage = message;
            }
        };
        run_1.run(skillContext)
            .then((result) => {
            assert.notEqual(loggedMessage, null);
            done();
        });
    });
});
//# sourceMappingURL=run.spec.js.map