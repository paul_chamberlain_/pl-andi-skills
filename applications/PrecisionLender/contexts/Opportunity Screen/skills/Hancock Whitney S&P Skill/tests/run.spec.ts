import * as assert from 'assert';
import { run } from '../run';
import * as andiSkills from 'andiskills';
import _ = require('underscore');

class RequestService {
    public get = (url: string, headers: any): Promise<any> =>{
        return this.makeRequest(url, headers, 'GET', null, false).then((res) => {
            return res;
        })
    }

    public post = (url: string, headers: any, body: any, stringifyBodyToJson: boolean = true): Promise<any> =>{
        return this.makeRequest(url, headers, 'POST', body, stringifyBodyToJson);
    }

    // Assumes content-type is json.
    private makeRequest = function (url: string, headers: any, method: string, body: any, stringifyBodyToJson: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            var http = url.startsWith('https') ? require('https') : require('http');
            var port = url.startsWith('https') ? '443' : '80';
            url = url.replace('http://', '').replace('https://', '');
            var portMatch = /\:\d{2,5}/.exec(url);
            if (portMatch !== null && portMatch.length > 0) {
                url = url.replace(portMatch[0], '');
                port = portMatch[0].replace(':', '');
            }

            var hostname = url.substring(0, url.indexOf('/'));
            var path = url.substring(url.indexOf('/'));

            var options = {
                hostname: hostname,
                path: path,
                port: port,
                method: method,
                headers: _.extend({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }, headers)
            };

            const req = http.request(options,
                function (res) {
                    res.setEncoding('utf8');
                    const data = [];
                    res.on('data', (chunk) => data.push(chunk));

                    res.on('end', () => {
                        try {
                            if (res.headers['content-type'] !== undefined && res.headers['content-type'].indexOf('json') > -1)
                                resolve(JSON.parse(data.join('')));
                            resolve(data.join(''));
                        } catch (e) {
                            var err = e + ' Request Host: ' + options.hostname;
                            reject(err);
                        }
                    });
                });
            req.on('error', (err) => reject(err))

            // write data to request body
            if (body) 
                req.write(stringifyBodyToJson ? JSON.stringify(body) : body);
            req.end();
        });
    }
}

function getContext():andiSkills.ISkillContext{
    var request = new RequestService();
    return {
        event:{
            id:"",
            organizationId:"",
            applicationId:"",
            context:"",
            conversationId:"",
            userId:"",
            inboundChannel:{
                channelType: andiSkills.ChannelTypes.ImmediateConversational
            } as andiSkills.ISkillChannel,
            eventType:andiSkills.EventTypes.ApplicationEvent,
            rawText:"",
            applicationEventData: {
                engineModel:{
                    commercialLoanAccounts: [{
                        amount: 100000,
                        maturity: 60,
                        paymentType: 2,
                        product:{
                            name: "test"
                        },
                        riskRating:{
                            rating: "1a"
                        }
                    }]
                }
            }
        },
        organizationId:"",
        userId:"",
        externalLibraries:{_: _},
        powers:{
            andi:{
                request:{
                     get:request.get,
                     post:request.post
                }
            }
        } as andiSkills.IPowerSet,
        availableOutputChannels:[],
        skillMetadata:{} as andiSkills.ISkillMetadata,
        skillConfig:{},
        putMessage:function():Promise<andiSkills.ISkillActivity>{
            return Promise.resolve({} as andiSkills.ISkillActivity);
        },
        log(message:any):void{
        },
        
    } as andiSkills.ISkillContext;
}

describe("Run Tests", () => {

    // Defines a Mocha unit test
    it("Logs that it ran", (done) => {
        var context = getContext();
        context.organizationId = "21b952f8-508c-4842-9d0b-54890201cfb7";
        console.log(context.event.applicationEventData.LastModified)
        var loggedMessage = null;
        
        run(context)
        .then((result)=>{
            assert.notEqual(loggedMessage,null);
            done();
        });
    });
});
