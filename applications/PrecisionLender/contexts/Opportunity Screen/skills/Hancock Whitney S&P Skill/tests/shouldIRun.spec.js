"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const shouldIRun_1 = require("../shouldIRun");
describe("ShouldIRun Tests", () => {
    // Defines a Mocha unit test
    it("Logs that it tried to run", (done) => {
        var loggedMessage = null;
        var skillContext = {
            log(message) {
                loggedMessage = message;
            }
        };
        shouldIRun_1.shouldIRun(skillContext)
            .then((result) => {
            assert.notEqual(loggedMessage, null);
            done();
        });
    });
});
//# sourceMappingURL=shouldIRun.spec.js.map