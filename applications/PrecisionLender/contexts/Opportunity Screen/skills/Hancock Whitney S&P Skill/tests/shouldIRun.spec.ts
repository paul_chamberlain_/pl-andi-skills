import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';

describe("ShouldIRun Tests", () => {

    // Defines a Mocha unit test
    it("Logs that it tried to run", (done) => {
        var loggedMessage = null;
        var skillContext = {
            log(message:any):void{
                loggedMessage = message;
            }
        } as andiSkills.ISkillContext;
        
        shouldIRun(skillContext)
        .then((result)=>{
            assert.notEqual(loggedMessage,null);
            done();
        });
    });
});
