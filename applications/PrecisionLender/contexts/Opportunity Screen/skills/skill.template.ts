import * as andiSkills from 'andiskills';

export class Skill implements andiSkills.IAndiSkill {
    constructor() {
        var self = this;
    }
    
    public initialize(): Promise<andiSkills.ISkillInitializationResult> {
        var initializationResult = {
            successFullyInitialized: true
        } as andiSkills.ISkillInitializationResult;

        return Promise.resolve(initializationResult);
    }
    
    public shouldIRun(skillContext: andiSkills.ISkillContext): Promise<andiSkills.IShouldIRunResponse> {
        return Promise.resolve({ shouldIRun: true });
    }

    public run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
        return Promise.resolve()
            .then((result) => {
                return skillContext.putMessage(
                    { name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued },
                    { message: "", category: "" });
            }).catch((err) => {
                return skillContext.putMessage(
                    { name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued },
                    { message: "", category: "" });
            });
    }
}