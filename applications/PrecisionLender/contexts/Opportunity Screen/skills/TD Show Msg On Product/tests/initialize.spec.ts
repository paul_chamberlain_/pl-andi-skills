import * as assert from 'assert';
import { initialize } from '../initialize';
import * as andiSkills from 'andiskills';

describe("Initialize Tests", () => {

    // Defines a Mocha unit test
    it("returns as successfully initialized", (done) => {
        initialize()
        .then((result)=>{
            assert.equal(result.successFullyInitialized,true);
            done();
        });
    });
});
