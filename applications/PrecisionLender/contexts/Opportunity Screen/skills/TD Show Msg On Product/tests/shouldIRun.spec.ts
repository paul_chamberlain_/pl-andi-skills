import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'

describe("ShouldIRun Tests", () => {

    // Defines a Mocha unit test
    it("Logs that it tried to run", (done) => {
        var skillContext = andiExternal.PrecisionLenderMocks.Mocks.mockSkillContextForPrecisionLenderOpportunityChange() as andiSkills.ISkillContext;
        shouldIRun(skillContext)
        .then((result)=>{
            assert.notEqual(result,null);
            done();
        });
    });
});
