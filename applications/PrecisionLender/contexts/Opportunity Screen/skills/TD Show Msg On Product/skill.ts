﻿import andiSkills = require('andiSkills')
import _ = require('underscore')

// 
export class Skill implements andiSkills.IAndiSkill {
    constructor() {
        var self = this;
    }
    private productFamilyIds = ["acdd4fd7-24ee-45d5-849b-2ea0f411544b", "7f0b92a0-d377-451a-ac80-d4e8a3b5b8ed"];

    public initialize(): Promise<andiSkills.ISkillInitializationResult> {
        return Promise.resolve({ successFullyInitialized: true });
    }

    public shouldIRun(skillContext: andiSkills.ISkillContext): Promise<andiSkills.IShouldIRunResponse> {
        skillContext.log("Checking if I should run");

        // Test that the Event is a PrecisionLender Application Event
        let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
        let data = event.applicationEventData;
        if (event.applicationEventName === undefined || event.applicationEventName.indexOf("log-engine-change") === -1) {
            return Promise.resolve({ shouldIRun: false });
        }


        if (!data.engineModel.commercialLoanAccounts || data.engineModel.commercialLoanAccounts.length < 1)
            return Promise.resolve({ shouldIRun: false });

        
        let claFamily = data.engineModel.commercialLoanAccounts.filter(cla => {
            return this.productFamilyIds.indexOf(cla.product.productFamilyId) > -1;
        });

        if (!claFamily)
            return Promise.resolve({ shouldIRun: false });

        return Promise.resolve({ shouldIRun: true });
    }

    public run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
        let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
        let eventData = event.applicationEventData;
        let claFamily = eventData.engineModel.commercialLoanAccounts.filter(cla => {
            return this.productFamilyIds.indexOf(cla.product.productFamilyId) > -1;
        });
        let claIds = [];
        claFamily.forEach(cla => {
            claIds.push(cla.id);
        });
        // Andi Button Message
        return skillContext.powers.andi.fieldTag.sendFieldTag(
            andiSkills.FieldTagTypes.Info
            , "TDShowMsgOnProductLIBORScaledTaxExemptFloat" // What is this for?
            , 4
            , "Use this product when only the Index is reduced by a factor. The spread is the tax-exempt spread for the deal."
            , null
            , null
            , function (api, model, compareHash, custom) {                
                var data = model.applicationEventData;
                let foundClaProductFamilyId = false;
                var event = model.applicationEventData;
                if (custom && custom.loanIds && custom.loanIds.length > 0) {
                    //event.contextId always represents the current loan id on the opportunity page
                    foundClaProductFamilyId = custom.loanIds.indexOf(event.contextId) > -1;
                }
                return foundClaProductFamilyId;
            }
            , null
            , null
            , { loanIds: claIds });

    }
}