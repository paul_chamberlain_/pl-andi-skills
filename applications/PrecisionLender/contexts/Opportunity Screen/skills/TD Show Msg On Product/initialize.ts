import * as andiSkills from 'andiskills';

export function initialize(): Promise<andiSkills.ISkillInitializationResult> {
    return Promise.resolve({ successFullyInitialized: true });
}