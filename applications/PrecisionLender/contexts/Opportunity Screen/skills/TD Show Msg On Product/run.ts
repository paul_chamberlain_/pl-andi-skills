import * as andiSkills from 'andiskills';
import * as andiExternal from 'andiexternal'

export function run(skillContext: andiSkills.ISkillContext): Promise<andiSkills.ISkillActivity> {
    let productFamilyIds = skillContext.skillConfig.productFamilyIds.split(',');
    let event: andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;
    let eventData = event.applicationEventData;
    let claFamily = eventData.engineModel.commercialLoanAccounts.filter(cla => {
        return productFamilyIds.indexOf(cla.product.productFamilyId) > -1;
    });
    let claIds = [];
    claFamily.forEach(cla => {
        claIds.push(cla.id);
    });
    // Andi Button Message
    return skillContext.powers.andi.fieldTag.sendFieldTag(
        andiSkills.FieldTagTypes.Info
        , "TDShowMsgOnProductLIBORScaledTaxExemptFloat" // What is this for?
        , 4
        , "Use this product when only the Index is reduced by a factor. The spread is the tax-exempt spread for the deal."
        , null
        , null
        , function (api, model, compareHash, custom) {                
            var data = model.applicationEventData;
            let foundClaProductFamilyId = false;
            if (custom && custom.loanIds && custom.loanIds.length > 0) {
                //data.contextId always represents the current loan id on the opportunity page
                foundClaProductFamilyId = custom.loanIds.indexOf(data.contextId) > -1;
            }
            return foundClaProductFamilyId;
        }
        , null
        , null
        , { loanIds: claIds });

}