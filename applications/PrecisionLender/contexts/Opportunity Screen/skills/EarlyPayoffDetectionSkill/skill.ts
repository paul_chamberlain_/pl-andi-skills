import * as andiSkills from 'andiskills';

export class Skill implements andiSkills.IAndiSkill {
    constructor() {
        var self = this;
    }
    
    public initialize(): Promise<andiSkills.ISkillInitializationResult> {
        var initializationResult = {
            successFullyInitialized: true
        } as andiSkills.ISkillInitializationResult;

        return Promise.resolve(initializationResult);
    }
    
    public shouldIRun(skillContext:andiSkills.ISkillContext):Promise<andiSkills.IShouldIRunResponse>{
        skillContext.log("Checking if I should run");
        if (skillContext.event.eventType != andiSkills.EventTypes.ApplicationEvent) {  
            console.log(skillContext.event.eventType);
            return Promise.resolve({shouldIRun: false});
        }
        let anEvent:andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;    
        if (anEvent.applicationEventName == "IntegrationRunSaveEventsPublished") {
            return Promise.resolve({ shouldIRun: true  });  
        }
    
    
        return Promise.resolve({ shouldIRun: false  });   
    }

    public run(skillContext: andiSkills.ISkillContext) : Promise<andiSkills.ISkillActivity>{
        return this.getPrediction(skillContext)
        .then((result)=>{
            var userModel = new andiSkills.EmailUserModel("Thomas", "thepner@precisionlender.com");
            console.log(userModel);
            var message = new andiSkills.EmailMessageModel(
                andiSkills.EmailFrequencyTypes.OneMinute,
                [userModel],
                "andi@precisionlender.com",
                "Subject - Early Payoff Detection Skill Has Been Run",
                "<strong>html We have detected some loans that paid off early... = </strong>" + result,
                `footer`
            );
            console.log(message.body);
            
            return skillContext.putMessage(
                    { name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued },
                    { message: message, category: 'TTC MVP' });
        }).catch((err)=>{
            var userModel = new andiSkills.EmailUserModel("Thomas", "thepner@precisionlender.com");
            //console.log(userModel);
            var message = new andiSkills.EmailMessageModel(
                andiSkills.EmailFrequencyTypes.OneMinute,
                [userModel],
                "andi@precisionlender.com",
                "Subject - Early Payoff Detection Skill Has Been Run",
                JSON.stringify(err),
                `footer`
            );
            
            return skillContext.putMessage(
                    { name: andiSkills.ChannelNames.EmailChannel, channelType: andiSkills.ChannelTypes.QueuedMessage, statusType: andiSkills.StatusTypes.Queued },
                    { message: message, category: 'Early Payoff Detection MVP' });
        });
    }

    private getPrediction(skillContext:andiSkills.ISkillContext) :Promise<any>{
        // var myServiceInfo = require('C:/Users/ThomasHepner/Desktop/web_service_credentials.json'); 
        //let key = myServiceInfo.apikey;
        //let URL = myServiceInfo.URL;
        let key = "Sj2BjQuqFHrgUTsfqdRraCc1F0UQg7BShJZu1dCDY/o2Fxh8VczP+wq4CSZ+UXx+edIw3E63zO9vyFQRYa/PZQ==";
        let URL = "https://ussouthcentral.services.azureml.net/subscriptions/b7b3f313bcc84754aa71a8c9a910841c/services/b01c1c89163a4525b5bad1c45c8659c8/execute?api-version=2.0&format=swagger";
        let BearerKey = "Bearer "+ key;
    
        var input = {
            "Inputs": {
              "input1": [
                {
                  "ClientId": skillContext.organizationId, 
                  "LastModified": skillContext.event.applicationEventData.LastModified 
                }
              ]
            },
            "GlobalParameters": {}
        };
        console.log(input.Inputs.input1);
        return skillContext.powers.andi.request.post(URL, {"Authorization": BearerKey}, input)
        .then((result)=>{
            console.log(result.Results.output1);
            return result.Results.output1; 
        });
    }
}