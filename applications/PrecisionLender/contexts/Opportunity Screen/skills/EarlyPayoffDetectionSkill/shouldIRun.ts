import * as andiSkills from 'andiskills';
import { IApplicationEvent, EventTypes } from "andiskills";

// The Early Payoff Detection Skill should run when an RA run for any given Client is completed.
export function shouldIRun(skillContext:andiSkills.ISkillContext):Promise<andiSkills.IShouldIRunResponse>{
    skillContext.log("Checking if I should run");
    if (skillContext.event.eventType != andiSkills.EventTypes.ApplicationEvent) {  
        console.log(skillContext.event.eventType);
        return Promise.resolve({shouldIRun: false});
    }
    let anEvent:andiSkills.IApplicationEvent = skillContext.event as andiSkills.IApplicationEvent;    
    if (anEvent.applicationEventName == "IntegrationRunSaveEventsPublished") {
        return Promise.resolve({ shouldIRun: true  });  
    }


    return Promise.resolve({ shouldIRun: false  });   
}