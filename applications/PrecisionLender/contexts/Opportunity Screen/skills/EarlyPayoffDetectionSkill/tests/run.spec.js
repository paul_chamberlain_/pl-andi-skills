"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const run_1 = require("../run");
const andiSkills = require("andiskills");
const _ = require("underscore");
class RequestService {
    constructor() {
        this.get = (url, headers) => {
            return this.makeRequest(url, headers, 'GET', null, false).then((res) => {
                return res;
            });
        };
        this.post = (url, headers, body, stringifyBodyToJson = true) => {
            return this.makeRequest(url, headers, 'POST', body, stringifyBodyToJson);
        };
        // Assumes content-type is json.
        this.makeRequest = function (url, headers, method, body, stringifyBodyToJson) {
            return new Promise((resolve, reject) => {
                var http = url.startsWith('https') ? require('https') : require('http');
                var port = url.startsWith('https') ? '443' : '80';
                url = url.replace('http://', '').replace('https://', '');
                var portMatch = /\:\d{2,5}/.exec(url);
                if (portMatch !== null && portMatch.length > 0) {
                    url = url.replace(portMatch[0], '');
                    port = portMatch[0].replace(':', '');
                }
                var hostname = url.substring(0, url.indexOf('/'));
                var path = url.substring(url.indexOf('/'));
                var options = {
                    hostname: hostname,
                    path: path,
                    port: port,
                    method: method,
                    headers: _.extend({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }, headers)
                };
                const req = http.request(options, function (res) {
                    res.setEncoding('utf8');
                    const data = [];
                    res.on('data', (chunk) => data.push(chunk));
                    res.on('end', () => {
                        try {
                            if (res.headers['content-type'] !== undefined && res.headers['content-type'].indexOf('json') > -1)
                                resolve(JSON.parse(data.join('')));
                            resolve(data.join(''));
                        }
                        catch (e) {
                            var err = e + ' Request Host: ' + options.hostname;
                            reject(err);
                        }
                    });
                });
                req.on('error', (err) => reject(err));
                // write data to request body
                if (body)
                    req.write(stringifyBodyToJson ? JSON.stringify(body) : body);
                req.end();
            });
        };
    }
}
function getContext() {
    var request = new RequestService();
    return {
        event: {
            id: "",
            organizationId: "",
            applicationId: "",
            context: "",
            conversationId: "",
            userId: "",
            inboundChannel: {
                channelType: andiSkills.ChannelTypes.ImmediateConversational
            },
            eventType: andiSkills.EventTypes.InboundNaturalLanguage,
            rawText: "",
            applicationEventData: {
                LastModified: ""
            }
        },
        organizationId: "",
        userId: "",
        externalLibraries: { _: _ },
        powers: {
            andi: {
                request: {
                    get: request.get,
                    post: request.post
                }
            }
        },
        availableOutputChannels: [],
        skillMetadata: {},
        skillConfig: {},
        putMessage: function () {
            return Promise.resolve({});
        },
        log(message) {
        },
    };
}
describe("Early Payoff Detection MVP", () => {
    describe("getPrediction Tests", () => {
        it("If I pass in Gulf Coast Bank and Trust with 2017-09-19, I get 4 rows", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "21b952f8-508c-4842-9d0b-54890201cfb7";
            context.event.applicationEventData.LastModified = '2017-09-19';
            console.log(context.event.applicationEventData.LastModified);
            run_1.getPrediction(context)
                .then((result) => {
                console.log(result);
                assert.equal(result.length, 4);
                done();
            });
        });
        it("If I pass in garbage for client & lastmodified, I get zero rows", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "garbage garbage garbage garbage";
            context.event.applicationEventData.LastModified = "garbage garbage garbage garbage";
            run_1.getPrediction(context)
                .then((result) => {
                console.log(result.length);
                assert.equal(result.length, 0);
                done();
            });
        });
        it("If I pass in First National Bank of Omaha and 2017-09-16, I get 65.18765", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var emailSent = false;
            var context = getContext();
            context.organizationId = "96c60b8e-d2ca-48ce-a74a-32ee65a35cbc";
            context.event.applicationEventData.LastModified = "2017-09-16";
            run_1.getPrediction(context)
                .then((result) => {
                console.log(result.length);
                assert.equal(result.length, 268);
                done();
            });
        });
    });
    describe("Run Tests", () => {
        // Defines a Mocha unit test
        it("If I pass in whatever, a message is Queued to thepner@precisionlender.com", (done) => {
            var loggedMessage = null;
            var testMessage = null;
            var testEmail = "thepner@precisionlender.com";
            var emailSent = false;
            var context = getContext();
            context.putMessage = function (channel, message, action) {
                testMessage = message.message;
                return Promise.resolve({});
            };
            context.organizationId = "21b952f8-508c-4842-9d0b-54890201cfb7";
            context.event.applicationEventData.LastModified = '2017-09-19';
            console.log(context.putMessage);
            run_1.run(context)
                .then((result) => {
                var email = testMessage;
                console.log(email.messageStatus);
                console.log(email.toAddress[0].emailAddress);
                assert.equal(email.messageStatus, 'Queued');
                assert.equal(email.toAddress[0].emailAddress, testEmail);
                done();
            });
        });
    });
});
//# sourceMappingURL=run.spec.js.map