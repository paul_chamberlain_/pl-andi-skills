import * as assert from 'assert';
import { shouldIRun } from '../shouldIRun';
import * as andiSkills from 'andiskills';
import { IApplicationEvent, EventTypes } from "andiskills";

describe("Early Payoff Detection Skill MVP", () => {
    describe("ShouldIRun Tests", () => {
        var loggedMessage = null;
        var skillContext = {
            event: {
                eventType:EventTypes.ApplicationEvent,
                applicationEventName: "IntegrationRunSaveEventsPublished",
                applicationEventData:"more",
                id:"sdf",
                organizationId:"sdf",
                applicationId:"sd",
                context:"sd",
                conversationId:"lkj",
                eventMetaData:null,
                userId:"thepner",
                inboundChannel:null
            },
            log(message:any):void{
                loggedMessage = message;
            }
        } as andiSkills.ISkillContext;

        // Defines a Mocha unit test
        it("Logs that it tried to run", (done) => {           
            shouldIRun(skillContext)
            .then((result)=>{
                assert.notEqual(loggedMessage,null);
                done();
            });
        });

        it("Checks if it returns true when applicationEventName is IntegrationRunSaveEventsPublished", (done) => {           
            var shouldIRunResults = shouldIRun(skillContext)

            .then((result)=>{
                assert.equal(result.shouldIRun,true);
                done();
            });
        });
    });
});