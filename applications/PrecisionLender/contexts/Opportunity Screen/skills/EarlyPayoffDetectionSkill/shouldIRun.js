"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const andiSkills = require("andiskills");
// The Early Payoff Detection Skill should run when an RA run for any given Client is completed.
function shouldIRun(skillContext) {
    if (skillContext.event.eventType != andiSkills.EventTypes.ApplicationEvent) {
        console.log(skillContext.event.eventType);
        return Promise.resolve({ shouldIRun: false });
    }
    let anEvent = skillContext.event;
    if (anEvent.applicationEventName == "IntegrationRunSaveEventsPublished") {
        var whetherIShouldRun = true;
    }
    skillContext.log("Checking if I should run");
    return Promise.resolve({ shouldIRun: whetherIShouldRun });
}
exports.shouldIRun = shouldIRun;
//# sourceMappingURL=shouldIRun.js.map